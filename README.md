# Dawid Lisiecki - Http Client

Easy to use http client library.

## Installation

```
composer require dawidlisiecki/http-client
```

## Setup

```php
use DawidLisiecki\Client;

$client = Client::withoutAuth('http://example.com');

$client->get('/endpoint');
```

## Client class
### Without authorization
```php
$client = Client::withoutAuth('http://example.com');
```
### Basic authorization
```php
$client = Client::withBasicAuth('http://example.com', 'username', 'password');
```
### JWT authorization
```php
$client = Client::withJWTAuth('http://example.com', 'token');
```
### Api key authorization
```php
$client = Client::withAPIKey('http://example.com', 'api_key_name', 'api_key_value');
```

## Client methods

### GET
```php
$client->get('/endpoint', $data = []);
```
### POST
```php
$client->post('/endpoint', $data = []);
```
### PUT
```php
$client->put('/endpoint', $data = []);
```
### DELETE
```php
$client->delete('/endpoint', $data = []);
```

#### Arguments

| Params       | Type     | Required                                                     |
| ------------ | -------- | ------------------------------------------------------------ |
| `endpoint`   | `string` | yes                                                          |
| `data`       | `array`  | no                                                           |



