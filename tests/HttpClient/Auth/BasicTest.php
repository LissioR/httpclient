<?php

namespace DawidLisiecki\Tests;

use DawidLisiecki\HttpClient\Auth\Basic;
use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{
    public function testGetHeaders()
    {
        $userName = 'username';
        $password = 'password';
        $expected = ['Authorization: Basic ' . base64_encode("username:password")];

        $basicAuth = new Basic($userName, $password);

        $this->assertEquals($expected, $basicAuth->getHeaders());
    }
}