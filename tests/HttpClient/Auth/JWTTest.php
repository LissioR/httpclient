<?php

namespace DawidLisiecki\Tests;

use DawidLisiecki\HttpClient\Auth\JWT;
use PHPUnit\Framework\TestCase;

class JWTTest extends TestCase
{
    public function testGetHeaders()
    {
        $token = 'token';
        $basicAuth = new JWT($token);
        $expected = [
            'Authorization: Bearer ' . $token,
            'Content-Type: application/json',
        ];

        $this->assertEquals($expected, $basicAuth->getHeaders());
    }
}