<?php

namespace DawidLisiecki\Tests;

use DawidLisiecki\HttpClient\Auth\APIKey;
use PHPUnit\Framework\TestCase;

class APIKeyTest extends TestCase
{
    public function testGetHeaders()
    {
        $apiKeyName = 'api_key_name';
        $apiKeyValue = 'api_key_value';
        $expectedHeaders = ['api_key_name: api_key_value'];

        $apiKey = new APIKey($apiKeyName, $apiKeyValue);

        $this->assertEquals($expectedHeaders, $apiKey->getHeaders());
    }
}