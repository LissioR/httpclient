<?php

namespace DawidLisiecki\Tests;

use DawidLisiecki\Client;
use DawidLisiecki\HttpClient\Exception\ClientException;
use DawidLisiecki\HttpClient\HttpClient;
use DawidLisiecki\HttpClient\Response\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class ClientTest extends TestCase
{
    public function testWithBasicAuth()
    {
        $baseUrl = 'https://api.example.com';
        $username = 'username';
        $password = 'password';

        $client = Client::withBasicAuth($baseUrl, $username, $password);

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testWithJWTAuth()
    {
        $baseUrl = 'https://api.example.com';
        $token = 'token';

        $client = Client::withJWTAuth($baseUrl, $token);

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testWithAPIKey()
    {
        $baseUrl = 'https://api.example.com';
        $apiKeyName = 'api_key_name';
        $apiKeyValue = 'api_key_value';

        $client = Client::withAPIKey($baseUrl, $apiKeyName, $apiKeyValue);

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testWithoutAuth()
    {
        $baseUrl = 'https://api.example.com';

        $client = Client::withoutAuth($baseUrl);

        $this->assertInstanceOf(Client::class, $client);
    }

    public function testGet()
    {
        $endpoint = '/endpoint';
        $client = Client::withoutAuth('http://example.com');

        $httpClientMock = $this->createMock(HttpClient::class);
        $httpClientMock->expects($this->once())
            ->method('request')
            ->with($endpoint)
            ->willReturn($this->createMock(Response::class));

        $client->httpClient = $httpClientMock;

        $this->assertInstanceOf(Response::class, $client->get($endpoint));
    }

    public function testPost()
    {
        $endpoint = '/endpoint';
        $client = Client::withoutAuth('http://example.com');

        $httpClientMock = $this->createMock(HttpClient::class);
        $httpClientMock->expects($this->once())
            ->method('request')
            ->with($endpoint)
            ->willReturn($this->createMock(Response::class));

        $client->httpClient = $httpClientMock;

        $this->assertInstanceOf(Response::class, $client->post($endpoint));
    }

    public function testPut()
    {
        $endpoint = '/endpoint';
        $client = Client::withoutAuth('http://example.com');

        $httpClientMock = $this->createMock(HttpClient::class);
        $httpClientMock->expects($this->once())
            ->method('request')
            ->with($endpoint)
            ->willReturn($this->createMock(Response::class));

        $client->httpClient = $httpClientMock;

        $this->assertInstanceOf(Response::class, $client->put($endpoint));
    }

    public function testDelete()
    {
        $endpoint = '/endpoint';
        $client = Client::withoutAuth('http://example.com');

        $httpClientMock = $this->createMock(HttpClient::class);
        $httpClientMock->expects($this->once())
            ->method('request')
            ->with($endpoint)
            ->willReturn($this->createMock(Response::class));

        $client->httpClient = $httpClientMock;

        $this->assertInstanceOf(Response::class, $client->delete($endpoint));
    }


    public function testRequestThrowsClientException()
    {
        $endpoint = '/endpoint';
        $client = Client::withoutAuth('http://exaple.com');

        $httpClientMock = $this->createMock(HttpClient::class);
        $httpClientMock->expects($this->once())
            ->method('request')
            ->with($endpoint)
            ->will($this->throwException($this->createMock(ClientException::class)));

        $client->httpClient = $httpClientMock;
        $this->expectException(ClientException::class);

        $client->get($endpoint);
    }
}