<?php
namespace DawidLisiecki\HttpClient;

use DawidLisiecki\HttpClient\Auth\AuthMethod;
use DawidLisiecki\HttpClient\Exception\ClientException;
use DawidLisiecki\HttpClient\Request\Request;
use DawidLisiecki\HttpClient\Response\Response;
use DawidLisiecki\HttpClient\Stream\StringStreamFactory;

class HttpClient
{
    public string $baseUrl;
    public ?AuthMethod $authMethod;
    public Request $request;

    /**
     * HttpClient constructor.
     *
     * @param string $baseUrl
     * @param AuthMethod|null $authMethod
     */
    public function __construct(string $baseUrl, AuthMethod $authMethod = null)
    {
        $this->baseUrl = $baseUrl;
        $this->authMethod = $authMethod;
        $this->request = new Request(new StringStreamFactory());
    }

    /**
     * @param string $endpoint
     * @param array $data
     * @param string $method
     *
     * @return Response
     *
     * @throws ClientException
     */
    public function request(string $endpoint, array $data = [], string $method = 'GET'): Response
    {
        try {
            $url = $this->baseUrl . $endpoint;
            $headers = $this->authMethod ? $this->authMethod->getHeaders() : [];

            return $this->request->send($url, $method, $data, $headers);
        } catch (\Exception $e) {
            throw new ClientException($e->getMessage(), $e->getCode());
        }
    }
}

