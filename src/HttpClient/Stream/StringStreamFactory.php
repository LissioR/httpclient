<?php
namespace DawidLisiecki\HttpClient\Stream;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\StreamFactoryInterface;

class StringStreamFactory implements StreamFactoryInterface
{
    public function createStream(string $content = ''): StreamInterface
    {
        return new StringStream($content);
    }

    public function createStreamFromFile(string $filename, string $mode = 'r'): StreamInterface
    {
        throw new \RuntimeException('Not implemented');
    }

    public function createStreamFromResource($resource): StreamInterface
    {
        throw new \RuntimeException('Not implemented');
    }
}
