<?php
namespace DawidLisiecki\HttpClient\Stream;

use Psr\Http\Message\StreamInterface;

class StringStream implements StreamInterface
{
    private string $string;
    private ?int $size;

    /**
     * StringStream constructor.
     *
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
        $this->size = strlen($string);
    }

    public function __toString(): string
    {
        return $this->string;
    }

    public function close(): void
    {
        $this->string = null;
        $this->size = null;
    }

    public function detach()
    {
        $this->close();
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function tell(): int
    {
        return 0;
    }

    public function eof(): bool
    {
        return true;
    }

    public function isSeekable(): bool
    {
        return false;
    }

    public function seek(int $offset, int $whence = SEEK_SET): void
    {
        throw new \RuntimeException('Cannot seek a StringStream');
    }

    public function rewind(): void
    {
        throw new \RuntimeException('Cannot rewind a StringStream');
    }

    public function isWritable(): bool
    {
        return false;
    }

    public function write(string $string): int
    {
        throw new \RuntimeException('Cannot write to a StringStream');
    }

    public function isReadable(): bool
    {
        return true;
    }

    public function read(int $length): string
    {
        return substr($this->string, 0, $length);
    }

    public function getContents(): string
    {
        return $this->string;
    }

    public function getMetadata(?string $key = null): ?array
    {
        if ($key === null) {
            return [];
        }
        return null;
    }
}
