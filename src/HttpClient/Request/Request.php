<?php

namespace DawidLisiecki\HttpClient\Request;

use DawidLisiecki\HttpClient\Exception\ClientException;
use DawidLisiecki\HttpClient\Response\Response;
use DawidLisiecki\HttpClient\Stream\StringStreamFactory;

class Request
{
    private StringStreamFactory $stringStreamFactory;

    public function __construct(StringStreamFactory $stringStreamFactory)
    {
        $this->stringStreamFactory = $stringStreamFactory;
    }

    /**
     * @param string $url
     * @param string $method
     * @param array $data
     * @param array $headers
     *
     * @return Response
     *
     * @throws ClientException
     */
    public function send(string $url, string $method, array $data = [], array $headers = []): Response
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        if (!empty($this->data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->data));
        }

        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curlHeaders = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        if (curl_errno($ch)) {
            throw new ClientException(curl_error($ch));
        }

        curl_close($ch);

        return new Response(
            $httpCode,
            $this->getHeadersAsArray($curlHeaders),
            $this->stringStreamFactory->createStream($result)
        );
    }

    /**
     * @param string $headers
     *
     * @return array
     */
    private function getHeadersAsArray(string $headers): array {
        $headerLines = explode(PHP_EOL, $headers);

        $arrayHeader = array_map(function ($headerLine) {
            if (strpos($headerLine, ':') === false) {
                return null;
            }

            list($name, $value) = explode(':', $headerLine, 2);

            return [$name => trim($value)];
        }, $headerLines);

        return array_filter($arrayHeader);
    }
}
