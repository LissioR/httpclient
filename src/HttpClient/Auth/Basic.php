<?php
namespace DawidLisiecki\HttpClient\Auth;

class Basic implements AuthMethod
{
    private string $username;
    private string $password;

    /**
     * Basic constructor.
     * @param string $username
     * @param string $password
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [
            'Authorization: Basic ' . base64_encode($this->username . ':' . $this->password)
        ];
    }
}