<?php
namespace DawidLisiecki\HttpClient\Auth;

class APIKey implements AuthMethod
{
    private string $apiKeyName;
    private string $apiKeyValue;

    /**
     * APIKey constructor.
     *
     * @param string $apiKeyName
     * @param string $apiKeyValue
     */
    public function __construct(string $apiKeyName, string $apiKeyValue)
    {
        $this->apiKeyName = $apiKeyName;
        $this->apiKeyValue = $apiKeyValue;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [
            $this->apiKeyName  . ': ' . $this->apiKeyValue
        ];
    }
}