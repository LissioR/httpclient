<?php
namespace DawidLisiecki\HttpClient\Auth;

class JWT implements AuthMethod
{
    private string $token;

    /**
     * JWT constructor.
     *
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/json',
        ];
    }
}