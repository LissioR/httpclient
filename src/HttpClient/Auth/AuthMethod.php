<?php
namespace DawidLisiecki\HttpClient\Auth;

interface AuthMethod
{
    /**
     * @return array
     */
    public function getHeaders(): array;
}