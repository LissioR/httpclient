<?php
namespace DawidLisiecki;

use DawidLisiecki\HttpClient\Auth\APIKey;
use DawidLisiecki\HttpClient\Auth\AuthMethod;
use DawidLisiecki\HttpClient\Auth\Basic;
use DawidLisiecki\HttpClient\Auth\JWT;
use DawidLisiecki\HttpClient\Exception\ClientException;
use DawidLisiecki\HttpClient\HttpClient;
use Psr\Http\Message\ResponseInterface;

class Client
{
    public string $baseUrl;
    public HttpClient $httpClient;

    /**
     * Client constructor.
     *
     * @param string $baseUrl
     * @param AuthMethod|null $authMethod
     */
    public function __construct(string $baseUrl, AuthMethod $authMethod = null)
    {
        $this->baseUrl = $baseUrl;
        $this->httpClient = new HttpClient($baseUrl, $authMethod);
    }

    /**
     * @param string $baseUrl
     * @param string $username
     * @param string $password
     *
     * @return self
     */
    public static function withBasicAuth(string $baseUrl, string $username, string $password): self
    {
        return new self($baseUrl, new Basic($username, $password));
    }

    /**
     * @param string $baseUrl
     * @param string $token
     *
     * @return self
     */
    public static function withJWTAuth(string $baseUrl, string $token): self
    {
        return new self($baseUrl, new JWT($token));
    }

    /**
     * @param string $baseUrl
     * @param string $apiKeyName
     * @param string $apiKeyValue
     *
     * @return self
     */
    public static function withAPIKey(string $baseUrl, string $apiKeyName, string $apiKeyValue): self
    {
        return new self($baseUrl, new APIKey($apiKeyName, $apiKeyValue));
    }

    /**
     * @param string $baseUrl
     *
     * @return self
     */
    public static function withoutAuth(string $baseUrl): self
    {
        return new self($baseUrl);
    }

    /**
     * @param string $endpoint
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws ClientException
     */
    public function get(string $endpoint, array $data = []): ResponseInterface
    {
        return $this->httpClient->request($endpoint, $data);
    }

    /**
     * @param string $endpoint
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws ClientException
     */
    public function post(string $endpoint, array $data = []): ResponseInterface
    {
        return $this->httpClient->request($endpoint, $data, 'POST');
    }

    /**
     * @param string $endpoint
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws ClientException
     */
    public function put(string $endpoint, array $data = []): ResponseInterface
    {
        return $this->httpClient->request($endpoint, $data, 'PUT');
    }

    /**
     * @param string $endpoint
     * @param array $data
     *
     * @return ResponseInterface
     *
     * @throws ClientException
     */
    public function delete(string $endpoint, array $data = []): ResponseInterface
    {
        return $this->httpClient->request($endpoint, $data, 'DELETE');
    }
}